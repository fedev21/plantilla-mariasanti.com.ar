<?php

//Thumbnails
function mariasanti_setup() {
	add_theme_support('post-thumbnails');
	add_theme_support('title-tag');

//Tamaño de imagen
	add_image_size('slider', 900, 525, true);
	add_image_size('flexbin', 0 , 350, true);


};
add_action('after_setup_theme','mariasanti_setup');

//Estilos

function mariasanti_styles() {
	//Registrar CSS
	wp_register_style('google_fonts', 'https://fonts.googleapis.com/css?family=Montserrat:400,500', array(), '1.0.0');
	wp_register_style('style', get_template_directory_uri(). '/style.css', array(), '1.0.0');
	wp_register_style('style-original', get_template_directory_uri(). '/css/style.css', array(), '1.0.0');
	wp_register_style('font-awesome', get_template_directory_uri(). '/css/font-awesome.min.css', array(), '1.0.0');
	wp_register_style('sandbox', get_template_directory_uri(). '/css/sandbox.css', array(), '1.0.0');
	wp_register_style('drawerCSS', get_template_directory_uri(). '/css/drawer.css', array(), '1.0.0');
	wp_register_style('fa-solid', get_template_directory_uri(). '/css/solid.min.css', array(), '1.0.0');
	wp_register_style('flexbin', get_template_directory_uri(). '/css/flexbin.css', array(), '1.0.0');
	wp_register_style('magnific-popupCSS', get_template_directory_uri(). '/css/magnific-popup.css', array(), '1.0.0');

	if(is_page(array('shows', 'news', 'projects'))):
		wp_enqueue_style('flexbin');
	endif;
	if(is_singular(array('show', 'new', 'project', 'painting', 'drawing'))):
		wp_enqueue_style('magnific-popupCSS');
		wp_enqueue_style('flexbin');
	endif;

	//Llamar CSS
	
	wp_enqueue_style('sandbox');
	wp_enqueue_style('fa-solid');
	wp_enqueue_style('drawerCSS');
	wp_enqueue_style('style');
	wp_enqueue_style('font-awesome');
	wp_enqueue_style('google_fonts');
	wp_enqueue_style('style-original');

	//Registrar JS
	wp_enqueue_script('bootstrap', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js', array(), '3.7.0', true );
	wp_enqueue_script('drawerJS', get_template_directory_uri(). '/js/drawer.min.js', array(), '1.0.0', true );
	wp_enqueue_script('iScroll', 'https://cdnjs.cloudflare.com/ajax/libs/iScroll/5.2.0/iscroll-infinite.js', array(), '1.0.0', true );
	if(is_page('inicio')):
		wp_enqueue_script('responsiveslides.min', get_template_directory_uri(). '/js/responsiveslides.min.js', array(), '1.0.0', true);
		wp_enqueue_script('Slider', get_template_directory_uri(). '/js/rslides.js', array(), '1.0.0', true);
	endif;
	if(is_singular(array('show', 'new', 'project', 'painting', 'drawing'))):
		wp_enqueue_script('magnific-popupJS.min', get_template_directory_uri(). '/js/jquery.magnific-popup.min.js', array(), '1.0.0', true);
		wp_enqueue_script('magnific-popup', get_template_directory_uri(). '/js/magnific-popup.js', array(), '1.0.0', true);
	endif;
	wp_enqueue_script('scripts', get_template_directory_uri(). '/js/scripts.js', array(), '1.0.0', true);
};

add_action('wp_enqueue_scripts', 'mariasanti_styles');

// Menus

function mariasanti_menus(){
	register_nav_menus(array(
		'header-menu' => __('Header Menu', 'mariasanti')
	));
}
add_action('init', 'mariasanti_menus');

// Quitar Admin Bar
add_filter( 'show_admin_bar', '__return_false' );

// Quitar Width y Height
add_filter( 'post_thumbnail_html', 'eliminar_att_ancho_alto', 10 );
add_filter( 'image_send_to_editor', 'eliminar_att_ancho_alto', 10 );
 
function eliminar_att_ancho_alto( $html ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}
