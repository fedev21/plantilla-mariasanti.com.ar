<?php 
/*
 * Template Name: News
 * Template Post Type: new
 */
 get_header(); ?>
	<main>
    <h1><a href="<?php echo esc_url( home_url('/') ); ?>">Maria Santi</a></h1>
    <?php if ( get_post_gallery() ){ ?>
        <div class="galeria flexbin flexbin-margin">
            <?php
            // Obtener galeria
            $galeria = get_post_gallery( get_the_ID(), false );

            // Obtener id
            $galeria_imagenes_ID = explode(',', $galeria['ids']);

            $i = 1;
            foreach ($galeria_imagenes_ID as $id):
                $imagenThumb = wp_get_attachment_image_src( $id,'flexbin')[0];
                $imagen = wp_get_attachment_image_src( $id,'full')[0]
             ?>
             <a href="<?php echo $imagen; ?>">
                <img src="<?php echo $imagenThumb; ?>" alt="">
            </a>
           <?php $i++; endforeach;?>
            
        </div>
        <div class="textos">
            <?php the_field('descripcion_projects'); ?>
        </div>
    <?php } else{ 
        while(have_posts()):the_post();
        the_content();
        the_field('descripcion_projects');
        endwhile;  
    } ?>
	</main>
<?php get_footer(); ?>