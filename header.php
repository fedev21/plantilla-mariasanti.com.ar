<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="description" content="<?php bloginfo('description'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
</head>
<body class="drawer drawer--left drawer--sidebar">
  <header>

    <button type="button" class="drawer-toggle drawer-hamburger">
      <span class="sr-only">toggle navigation</span>
      <span class="drawer-hamburger-icon"></span>
    </button>
    
    <nav class="drawer-nav">
      <h1><a href="<?php echo esc_url( home_url('/') ); ?>">Maria Santi</a></h1>

      <?php 
      $args = array(
        'theme_location' => 'header-menu',
        'menu_class' => 'drawer-menu',
        'items_wrap' => '<ul>%3$s</ul>',
        'link_before' => '<span class="bg">',
        'link_after' => '</span>'
      );
      wp_nav_menu($args);
     ?>

      <!--. Menu Header -->
      <div class="social">
          <li><a href="https://www.instagram.com/mariasantiart/" target="_blank" class="redes"><img src="<?php echo get_template_directory_uri(); ?>/img/instagram.svg"></a></li>
          <li><a href="https://www.facebook.com/www.mariasanti.com.ar" target="_blank" class="redes"><img src="<?php echo get_template_directory_uri(); ?>/img/facebook.svg"></a></li>
          <li><a href="https://ar.pinterest.com/mariusanti/" target="_blank" class="redes"><img src="<?php echo get_template_directory_uri(); ?>/img/pinterest.svg"></a></li>
        </div><!--. Redes Sociales -->
    </nav>
  </header>