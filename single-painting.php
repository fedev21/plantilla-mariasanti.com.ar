<?php 
/*
 * Template Name: Paintings
 * Template Post Type: painting
 */
 get_header(); ?>
	<main>
  <h1><a href="<?php echo esc_url( home_url('/') ); ?>">Maria Santi</a></h1>
        <div class="galeria ampliadas-centradas">
            <?php
            // Obtener galeria
            $galeria = get_post_gallery( get_the_ID(), false );
            // Obtener id
            $galeria_imagenes_ID = explode(',', $galeria['ids']);

            $i = 1;
            foreach ($galeria_imagenes_ID as $id):
                $imagenThumb = wp_get_attachment_image_src( $id,'flexbin')[0];
                $imagen = wp_get_attachment_image_src( $id,'full')[0];
                $imagenTitulo = get_the_title($id);
             ?>
            <div class="rowObra">
              <div class="obra">
                <a href="<?php echo $imagen; ?>">
                  <img src="<?php echo $imagen; ?>">
                  <div class="tituloObra"><span><?php echo $imagenTitulo; ?></span></div>
                </a>
              </div>
            </div>
           <?php $i++; endforeach;?>
            
        </div>
	</main>
<?php get_footer(); ?>