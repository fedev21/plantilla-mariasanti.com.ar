<?php get_header(); ?>
	<main>	
	<h1><a href="<?php echo esc_url( home_url('/') ); ?>">Maria Santi</a></h1>
		<div class="fotos">
	    	<ul class="rslides">
	            <?php 
	                $args = array(
	                    'post_type' => 'slider',
	                    'posts_per_page' => -1,
	                    'orderby' => 'name',
	                    'order' => 'ASC',
	                );
	                $slider = new WP_Query($args);
	                while($slider->have_posts()): $slider->the_post();
	            ?>
		            <li>
		                <?php the_post_thumbnail('slider'); ?>
		            </li>
	            <?php endwhile; wp_reset_postdata(); ?>
	    	</ul>
	    </div>
	</main>
<?php get_footer(); ?>