<?php get_header(); ?>

	<main>
    <h1><a href="<?php echo esc_url( home_url('/') ); ?>">Maria Santi</a></h1>
        <div class="textos">
            <?php
            while(have_posts()):the_post();
            the_post_thumbnail();
            the_title('<h2>', '</h2>');
            the_content();

            endwhile; 
         ?>
        </div>
	</main>
<?php get_footer(); ?>

