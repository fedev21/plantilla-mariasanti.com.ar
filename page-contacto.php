<?php 
/*
* Template Name: Contacto
*/

get_header(); ?>
<main>
	<h1><a href="<?php echo esc_url( home_url('/') ); ?>">Maria Santi</a></h1>
	<div class="textos">
		<?php while(have_posts()): the_post(); ?>

			<?php the_content(); ?>

		<?php endwhile; ?>
	</div>
</main>
<?php get_footer(); ?>