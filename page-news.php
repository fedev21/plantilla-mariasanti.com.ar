<?php 
/*
* Template Name: News
*/

get_header(); ?>
	<main>
    <h1><a href="<?php echo esc_url( home_url('/') ); ?>">Maria Santi</a></h1>
        <div class="galeria flexbin flexbin-margin">
            <?php 
                $args = array(
                    'post_type' => 'new',
                    'posts_per_page' => -1,
                    'orderby' => 'date',
                    'order' => 'DESC'
                );
                $news = new WP_Query($args);
                while($news->have_posts()): $news->the_post();
            ?>
            <a href="<?php the_permalink(); ?>">
                <?php the_post_thumbnail('flexbin'); ?>
                <div class="tituloObra"><span><?php the_title(); ?></span></div>
            </a>
            <?php endwhile; wp_reset_postdata(); ?>
        </div>
	</main>
<?php get_footer(); ?>